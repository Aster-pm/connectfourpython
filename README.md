# ConnectFourPython

A connect four minigame in which you can play against an IA based on minimax alpha-beta pruning algorithm. The AI will be represented by "the machine" (player 1) and you will embody the human (player -1). The player who starts is chosen randomly, the game ends either when someone managed to align 4 piece in a row or when the board is full.

The algorithm includes a depth counter allowing you to define how deep you want it to search for a move, the higher the number the stronget the AI will be (but the longer the compute time)

## Usage
You can start it using python3 in shell or using some specific IDE (Thonny for example)
```sh 
connectFourPython$ python3 connect4.py
```


## Authors and acknowledgment
### Authors
- Tom Taffin
- Nassim Guinet

We work as a team of 2 on this project, we took the basis from another AI project (tictactoe project using a similar alpha-beta pruning algorithm)

## Project status
There is no plan for future update on this project, it was mainly done by curiosity. Though there's a lot to improve, the alphabeta method is not optimize and it could be 2x faster with a better variable management. I guess you could also gain some memory on the wins tab if instead you write an arithmetic function that returns true the actual state of the board is a winning state rather than looking into each winning possible state and if the board is in this state or not.