import numpy as np


class Board:
    def __init__(self):
        self.board = np.zeros(42, dtype=int)
        self.render = self.board.reshape((6,7))

        # each possible 4 in a row winning pieces
        self.wins = [
    [0, 1, 2, 3],
    [41, 40, 39, 38],
    [7, 8, 9, 10],
    [34, 33, 32, 31],
    [14, 15, 16, 17],
    [27, 26, 25, 24],
    [21, 22, 23, 24],
    [20, 19, 18, 17],
    [28, 29, 30, 31],
    [13, 12, 11, 10],
    [35, 36, 37, 38],
    [6, 5, 4, 3],
    [0, 7, 14, 21],
    [41, 34, 27, 20],
    [1, 8, 15, 22],
    [40, 33, 26, 19],
    [2, 9, 16, 23],
    [39, 32, 25, 18],
    [3, 10, 17, 24],
    [38, 31, 24, 17],
    [4, 11, 18, 25],
    [37, 30, 23, 16],
    [5, 12, 19, 26],
    [36, 29, 22, 15],
    [6, 13, 20, 27],
    [35, 28, 21, 14],
    [0, 8, 16, 24],
    [41, 33, 25, 17],
    [7, 15, 23, 31],
    [34, 26, 18, 10],
    [14, 22, 30, 38],
    [27, 19, 11, 3],
    [35, 29, 23, 17],
    [6, 12, 18, 24],
    [28, 22, 16, 10],
    [13, 19, 25, 31],
    [21, 15, 9, 3],
    [20, 26, 32, 38],
    [36, 30, 24, 18],
    [5, 11, 17, 23],
    [37, 31, 25, 19],
    [4, 10, 16, 22],
    [2, 10, 18, 26],
    [39, 31, 23, 15],
    [1, 9, 17, 25],
    [40, 32, 24, 16],
    [9, 17, 25, 33],
    [8, 16, 24, 32],
    [11, 17, 23, 29],
    [12, 18, 24, 30],
    [1, 2, 3, 4],
    [5, 4, 3, 2],
    [8, 9, 10, 11],
    [12, 11, 10, 9],
    [15, 16, 17, 18],
    [19, 18, 17, 16],
    [22, 23, 24, 25],
    [26, 25, 24, 23],
    [29, 30, 31, 32],
    [33, 32, 31, 30],
    [36, 37, 38, 39],
    [40, 39, 38, 37],
    [7, 14, 21, 28],
    [8, 15, 22, 29],
    [9, 16, 23, 30],
    [10, 17, 24, 31],
    [11, 18, 25, 32],
    [12, 19, 26, 33],
    [13, 20, 27, 34],
  ]


    def actions(self):
        """gives you every possible actions by checking each position of the top most row

        Returns:
            Array : each row in which you can place your piece
        """
        acts = []
        for i in range (0,7):
            if self.board[i]==0:
                acts.append(i)
        return acts


    def trans(self, act, player):
        """place a piece in the given column (act)

        Args:
            act (int): the index of the column
            player (int): the player (1 for the Machine/IA, -1 for the human player) 
        """
        for i in range(len(self.board)-(7-act),-1,-7):
            if(self.board[i]==0):
                self.board[i] = player
                break
    
    def undo(self, act):
        """remove the top piece in the given column (act)

        Args:
            act (int): the index of the column
        """
        for i in range(len(self.board)-(7-act),-1,-7):
            if(self.board[i]==0):
                self.board[i+7] = 0
                break
        self.board[act] = 0
        
    def final(self):
        """Checks if the board is in a final state, aka 4 pieve of the same player are align or the board is full

        Returns:
            boolean : true if in final state, false else
        """
        return len(self.actions()) == 0 or self.winner() 

    def winner(self):
        """checks if someone won the game by looking at every winning combination

        Returns:
            boolean: true if someone won, false else
        """
        for tab in self.wins :
            compteur = 0
            for index in tab :
                if index < len(self.board):
                    compteur += self.board[index]
            if (abs(compteur) == 4):
                return True
        return False
    
    def utility(self, player):
        """gives the utility score of the current board (called after simulating a move for the AI)

        Args:
            player (int): the player doing the move

        Returns:
            int: the utility score
        """
        if self.winner():
            return -player   
        else:
            return 0

    def display(self):
        print(self.render)


    def mini_alphabeta(self, alpha, beta, player,maxR):
        """Minimax alpha-beta pruning algorithm - This method will return the move with the minimal gain possible for the player using the alpha-beta pruning by checking the utility score of eache path.

        Args:
            alpha (int): the highest score
            beta (int): the lowest score
            player (int): the player playing
            maxR (int): the max depth at which the algorithm will look for a value

        Returns:
            int: the move
        """

        maxR -= 1
        if self.final():
            return self.utility(player)
        elif maxR == 0:
            return -10
        else :
            value = np.infty
            for action in self.actions() :
                self.trans(action, player)
                value = min(self.maxi_alphabeta(alpha,beta,-player,maxR),value)
                self.undo(action) 
                if(alpha < value):
                    beta = min(beta, value)
                else:  
                    break
        return value

    def maxi_alphabeta(self, alpha, beta, player, maxR):
        """Minimax alpha-beta pruning algorithm - This method will return the move with the minimal gain possible for the player using the alpha-beta pruning by checking the utility score of eache path.

        Args:
            alpha (int): the highest score
            beta (int): the lowest score
            player (int): the player playing
            maxR (int): the max depth at which the algorithm will look for a value

        Returns:
            int: the move
        """

        maxR -= 1

        if self.final():
            return self.utility(player)
        elif maxR == 0:
            return 10
        else :
            value = -np.infty
            for action in self.actions() :
                self.trans(action, player)
                value = max(self.mini_alphabeta(alpha,beta,-player,maxR),value)
                self.undo(action) 
                if(value < beta):
                    alpha = max(alpha, value)
                else:  
                    break
        return value

    def alphabeta(self, player):
        """Setup alpha & beta and calls the alphabeta algorithm to compute the best output possible

        Args:
            player (int): the player playing

        Returns:
            int: the action chosen by the algorithm
        """
        alpha = -np.infty
        beta = np.infty
        playing = self.maxi_alphabeta(alpha,beta,player,7)
        for action in self.actions() :
                self.trans(action, player)
                value = self.mini_alphabeta(alpha,beta,-player,6)
                print(value)
                if(playing == value):
                    self.undo(action)
                    return action
                self.undo(action)
                
       


if __name__ == "__main__":
    b = Board()

    # choose the player who starts
    chance = np.random.randint(0, 2)
    if chance == 1:
        print("Human (-1) plays first")
        player = -1
    else:
        print("Machine (1) plays first")
        player = 1

    while True:
        b.display()
        
        actions = b.actions()
        print(f"Player {player}; actions : {actions}")
        if player == -1:
            pos = int(input("pos ? "))
            if pos not in actions:
                print("impossible move")
                break
        else:
            pos = b.alphabeta(player)
            print(f"Machine plays: {pos}")

        b.trans(pos, player)
        if b.final():
            if b.winner():
                print(f"Player {player} wins!")
            else:
                print("Tie!")
            break

        player = - player
    

